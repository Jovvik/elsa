# runs all tracking algorithms

import cv2
import sys
 
def createTracker(trackerType):
    if trackerType == 'BOOSTING':
        tracker = cv2.TrackerBoosting_create()
    if trackerType == 'MIL':
        tracker = cv2.TrackerMIL_create()
    if trackerType == 'KCF':
        tracker = cv2.TrackerKCF_create()
    if trackerType == 'TLD':
        tracker = cv2.TrackerTLD_create()
    if trackerType == 'MEDIANFLOW':
        tracker = cv2.TrackerMedianFlow_create()
    if trackerType == 'GOTURN':
        tracker = cv2.TrackerGOTURN_create()
    if trackerType == 'MOSSE':
        tracker = cv2.TrackerMOSSE_create()
    if trackerType == "CSRT":
        tracker = cv2.TrackerCSRT_create()
    return tracker

trackerTypes = ['MEDIANFLOW', 'CSRT'] # also GOTURN but i'm too lazy to download the caffe model
# trackerTypes = ['CSRT']
# trackerType = trackerTypes[2]

colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255), (0, 255, 255), (255, 255, 255), (0, 0, 0)]

# Read video
video = cv2.VideoCapture(0)

# Exit if video not opened.
if not video.isOpened():
    print ("Could not open video capture")
    sys.exit()

# Read first frame.
ok, frame = video.read()
if not ok:
    print ('Cannot read frame')
    sys.exit()

# Define an initial bounding box
# bbox = (287, 23, 86, 320)

# Uncomment the line below to select a different bounding box
bbox = cv2.selectROI(frame, False)

trackers = []
for trackerType in trackerTypes:
    trackers.append((trackerType, createTracker(trackerType), bbox))

# Initialize tracker with first frame and bounding box
for tracker in trackers:
    tracker[1].init(frame, bbox)

while True:
    # Read a new frame
    ok, frame = video.read()
    if not ok:
        break
    
    # Start timer
    timer = cv2.getTickCount()

    statuses = []
    # Update tracker
    for tracker in trackers:
        ok, bbox = tracker[1].update(frame)
        statuses.append((ok, bbox))

    # Calculate Frames per second (FPS)
    fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer)

    for idx, status in enumerate(statuses):
        ok = status[0]
        bbox = status[1]

        # Draw bounding box
        if ok:
            # Tracking success
            p1 = (int(bbox[0]), int(bbox[1]))
            p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
            cv2.rectangle(frame, p1, p2, colors[idx], 2, 1)
        else :
            # Tracking failure
            print("Tracking failure detecte in " + trackers[idx][0])

    # Display tracker type on frame
    # cv2.putText(frame, trackerType + " Tracker", (100,20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50),2)

    # Display FPS on frame
    cv2.putText(frame, "FPS : " + str(int(fps)), (100,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50), 2)

    # Display result
    cv2.imshow("Tracking", frame)

    # Exit if ESC pressed
    k = cv2.waitKey(1) & 0xff
    if k == 27 : break