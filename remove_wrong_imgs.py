import json
from copy import deepcopy

# res = deepcopy(data)
res = {}
cnt = 0

with open('dataset_v2/via_region_data_first.json', 'r') as f:
    data = json.load(f)

for img in data:
    if len(data[img]['regions']) != 0:
        cnt+=1
        res[img[:-5]] = data[img]
        # del res[img]

with open('dataset_v2/via_region_data_second.json', 'r') as f:
    data = json.load(f)

for img in data:
    if len(data[img]['regions']) != 0:
        res[img[:-5]] = data[img]
        cnt+=1
        # del res[img]
        
with open('dataset_v2/via_region_data_third.json', 'r') as f:
    data = json.load(f)

for img in data:
    if len(data[img]['regions']) != 0:
        cnt+=1
        res[img[:-5]] = data[img]
        # del res[img]

print(cnt)
with open('dataset_v2/filtered_via_region_data.json', 'w') as f2:
    json.dump(res, f2)