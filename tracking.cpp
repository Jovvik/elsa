// не работает, я обосрался где-то в классе

/*
Отче,
да святится имя Твое;
да приидет Царствие Твое;
хлеб наш насущный подавай нам на каждый день;
и прости нам грехи наши,
ибо и мы прощаем всякому должнику нашему;
и не введи нас в искушение,
но избавь нас от лукавого. 
Ибо Твое есть Царство и сила,
и слава во веки. Аминь.
*/

#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/core/ocl.hpp>

#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace cv;

Point2i midOfRect(Rect rect)
{
    return Point2i(int(rect.width + 2*rect.x) / 2, int(rect.height + 2*rect.y) / 2);
}

class trackerWrapper
{
private:
    Ptr<Tracker> tracker;
    Point2f pos;
    Point2f prevpos;
public:
    string name;
    Vec3i color;
    bool drawPath;
    bool ok;
    Mat path;
    Rect2d bbox;
    trackerWrapper(std::string name_, Vec3i color_, Mat frame, Rect bbox_, bool drawPath_=true)
    {
        name = name_;
        color = color_;
        drawPath = drawPath_;
        if (name == "BOOSTING")
            tracker = TrackerBoosting::create();
        else if (name == "MIL")
            tracker = TrackerMIL::create();
        else if (name == "KCF")
            tracker = TrackerKCF::create();
        else if (name == "TLD")
            tracker = TrackerTLD::create();
        else if (name == "MEDIANFLOW")
            tracker = TrackerMedianFlow::create();
        else if (name == "GOTURN") //! Warning! Requires stuff from here: https://github.com/Mogball/goturn-files
            tracker = TrackerGOTURN::create();
        else if (name == "MOSSE")
            tracker = TrackerMOSSE::create();
        else if (name == "CSRT")
            tracker = TrackerCSRT::create();
        else
            throw exception();
        this->bbox = bbox_;
        this->pos = midOfRect(bbox);
        if (drawPath)
            path = Mat::zeros(frame.size(), frame.type());
        tracker->init(frame, bbox);
    }

    void update(Mat frame)
    {
        cout << bbox << endl;
        ok = tracker->update(frame, this->bbox);
        cout << bbox << endl;
        this->prevpos = pos;
        this->pos = midOfRect(this->bbox);
        // cout << prevpos << pos << endl;
        if (drawPath)
            line(path, this->prevpos, this->pos, color);
    }
};

int main()
{

    // Read video capture
    VideoCapture cap = VideoCapture(0);

    // Exit if video capture not opened
    if (!cap.isOpened())
    {
        cout << "Could not open video capture" << endl;
        return 0;
    }

    // Read first frame.
    Mat frame;
    cap >> frame;
    if (frame.empty())
    {
        cout << "Cannot read frame" << endl;
        return 0;
    }

    // Uncomment the line below to select a different bounding box
    Rect2i bbox = selectROI(frame);

    vector<trackerWrapper> trackers;
    trackers.push_back(trackerWrapper("TLD", Vec3i(0, 255, 0), frame, bbox));
    trackers.push_back(trackerWrapper("CSRT", Vec3i(255, 0, 0), frame, bbox));
    // trackers.push_back(trackerWrapper("BOOSTING", Vec3i(0, 0, 255)));
    // trackers.push_back(trackerWrapper("MIL", Vec3i(255, 255, 0)));
    trackers.push_back(trackerWrapper("KCF", Vec3i(0, 0, 255), frame, bbox));
    // trackers.push_back(trackerWrapper("MEDIANFLOW", Vec3i(255, 0, 255)));
    // trackers.push_back(trackerWrapper("GOTURN", Vec3i(128, 128, 128)));
    // trackers.push_back(trackerWrapper("MOSSE", Vec3i(255, 255, 255)));

    // Initialize tracker with first frame and boundFaTrlseing box
    // for (auto tracker : trackers)
    //     tracker.init(frame, bbox);

    while (true)
    {
        // Read a new frame
        cap >> frame;
        
        if (frame.empty())
        {
            cout << "Cannot read frame" << endl;
            return 0;
        }
            
        // Start timer
        int64 timer = getTickCount();

        // Update tracker
        for (auto tracker : trackers)
            tracker.update(frame);

        // Calculate FPS
        double fps = getTickFrequency() / (getTickCount() - timer);

        for (auto tracker : trackers)
        {
            bbox = tracker.bbox;
            // Draw bounding box
            if (tracker.ok)
            {
                // Tracking success
                rectangle(frame, bbox, tracker.color, 2, 1);
                imshow("Path for " + tracker.name, tracker.path);
            }
            else
            {
                // Tracking failure
                putText(frame, "Tracking failure detected in " + tracker.name, Point(100,80), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0,0,255), 1);
            }
        }

        // Display tracker type on frame
        // cv2.putText(frame, tracker_type + " Tracker", (100,20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50),2)
        
        // Display FPS on frame
        putText(frame, "FPS : " + to_string(int(fps)), Point(100,50), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(50,170,50), 2);

        // Display result
        imshow("Tracking", frame);
        // cv2.imshow("Path", movementMap)

        // Exit if ESC pressed
        int k = waitKey(1);
        if (k == 27) 
        {
            cout << "ESC key was pressed, exiting..." << endl;
            return 0;
        }
    }
}