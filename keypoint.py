import numpy as np
import cv2
from matplotlib import pyplot as plt

MIN_MATCH_COUNT = 10

class imagePointDesc():
    def __init__(self, img, algo):
        self.img = img
        self.keypoints, self.descriptors = algo.detectAndCompute(img, None)

imgPaths = ['imgs/nya.jpg', 'imgs/zmhih.jpg', 'imgs/zhizha.jpg']
imgs = []
for imgPath in imgPaths:
    imgs.append(imagePointDesc(cv2.imread(imgPath), cv2.ORB_create()))

# FLANN params
FLANN_INDEX_LSH = 6
index_params= dict(algorithm = FLANN_INDEX_LSH,
                   table_number = 6, # 12
                   key_size = 12,     # 20
                   multi_probe_level = 1) #2

flann = cv2.FlannBasedMatcher(index_params)

# Create video capture
cap = cv2.VideoCapture(0)

while True:
    # Read a new frame
    ok, frame = cap.read()
    if not ok:
        break

    frame = imagePointDesc(frame, cv2.ORB_create())
    for idx, img in enumerate(imgs):
        if img.descriptors is None or frame.descriptors is None:
            continue
        if len(img.descriptors) < 2 or len(frame.descriptors) < 2:
            continue
        matches = flann.knnMatch(img.descriptors, frame.descriptors, k=2)

        good = []
        for x in matches:
            if len(x) > 1:
                m,n = x
                if m.distance < 0.7*n.distance:
                    good.append(m)

        if len(good) > MIN_MATCH_COUNT:
            src_pts = np.float32([img.keypoints[m.queryIdx].pt for m in good]).reshape(-1,1,2)
            dst_pts = np.float32([frame.keypoints[m.trainIdx].pt for m in good]).reshape(-1,1,2)

            M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
            matchesMask = mask.ravel().tolist()

            h,w,_ = img.img.shape
            pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
            dst = cv2.perspectiveTransform(pts,M)

            # img2 = cv2.polylines(frame,[np.int32(dst)],True,255,3, cv2.LINE_AA)

        else:
            print ("Not enough matches are found - %d/%d in %d" % (len(good), MIN_MATCH_COUNT, idx))
            matchesMask = None
        draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                        singlePointColor = None,
                        matchesMask = matchesMask, # draw only inliers
                        flags = 2)
        if img.img is not None and frame.img is not None:
            img3 = cv2.drawMatches(img.img,img.keypoints,frame.img,frame.keypoints,good,None,**draw_params)
            
            if img3 is not None:
                cv2.imshow('gray' + str(idx), img3)
            cv2.waitKey(1)