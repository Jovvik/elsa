import serial

import cv2
import sys
import numpy as np
from random import randint

class NXT():
    def __init__(self, port, baud=9600, timeout=0.02):
        self.ser = serial.Serial(port, baud, timeout=timeout)
    
    def send(self, vals_):
        vals = vals_.copy()
        for idx, val in enumerate(vals):
            vals[idx] = hex(val)[2:].zfill(4)
        buf = b'\x0A\x00\x80\x09\x00\x06'
        for val in vals:
            buf += bytes.fromhex(val[2:]) + bytes.fromhex(val[:-2])
        self.ser.write(buf)

def randColor():
    return (randint(0, 255), randint(0, 255), randint(0, 255))

def midOfRect(rect):
    return (int(rect[2] + 2*rect[0]) // 2, int(rect[3] + 2*rect[1]) // 2)

class trackerWrapper():
    def __init__(self, name, color=(255,0,0), drawPath=True):
        self.name = name
        self.color = color
        self.drawPath = drawPath
        if name == 'BOOSTING':
            self.tracker = cv2.TrackerBoosting_create()
        elif name == 'MIL':
            self.tracker = cv2.TrackerMIL_create()
        elif name == 'KCF':
            self.tracker = cv2.TrackerKCF_create()
        elif name == 'TLD':
            self.tracker = cv2.TrackerTLD_create()
        elif name == 'MEDIANFLOW':
            self.tracker = cv2.TrackerMedianFlow_create()
        elif name == 'GOTURN': #! Warning! Requires stuff from here: https://github.com/Mogball/goturn-files
            self.tracker = cv2.TrackerGOTURN_create()
        elif name == 'MOSSE':
            self.tracker = cv2.TrackerMOSSE_create()
        elif name == "CSRT":
            self.tracker = cv2.TrackerCSRT_create()
        else:
            raise Exception()

    def init(self, frame, bbox):
        self.firstpos = midOfRect(bbox)
        self.pos = midOfRect(bbox)
        if self.drawPath:
            self.path = np.zeros_like(frame)
        return self.tracker.init(frame, bbox)

    def update(self, frame):
        ok, bbox = self.tracker.update(frame)
        self.ok = ok
        self.bbox = bbox
        self.prevpos = self.pos
        self.pos = midOfRect(self.bbox)
        if self.drawPath:
            cv2.line(self.path, self.prevpos, self.pos, color=self.color, thickness=1)

class multiObjTracker():
    def __init__(self, name, NXTs, objNum):
        self.trackers = []
        self.NXTs = NXTs
        for _ in range(objNum):
            self.trackers.append(trackerWrapper(name, randColor()))
    
    def init(self, frame, bboxes):
        self.bboxes = bboxes
        for idx, tracker in enumerate(self.trackers):
            tracker.init(frame, bboxes[idx])
            tracker.name+=str(idx)

    def update(self, frame):
        for idx, tracker in enumerate(self.trackers):
            tracker.update(frame)
            print(tracker.pos[0] - tracker.firstpos[0], tracker.pos[1] - tracker.firstpos[1])