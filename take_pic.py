# takes pictures for a dataset

import cv2
import os

cnt = 170
cap = cv2.VideoCapture(1)

while True:
    ok, frame = cap.read()
    if ok:
        cv2.imshow('frame', frame)
        k = cv2.waitKey(1)
        if k == ord('a'):
            cnt+=1
            path = "dnnImgs/" + str(cnt) + '.png'
            while os.path.isfile(path):
                cnt += 1
                path = "dnnImgs/" + str(cnt) + '.png'

            print(path)
            cv2.imwrite(path, frame)