python2 via_to_tfrecord.py \
  --image_dir=dnnImgs/\
  --annotation_file=filtered_via_region_data.json \
  --label_map_file=label_map.pbtxt \
  --output=dataset.record \
  --t_v_ratio=1
