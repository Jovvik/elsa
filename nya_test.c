float angleA;
float angleB;
float angleC;

long x, y, z;

void pause()
{
	motor[motorA]=0;
	motor[motorB]=0;
	motor[motorC]=0;
}

void showSpeed()
{
	displayCenteredTextLine(3, "motorA: %d", motor[motorA]);
	displayCenteredTextLine(4, "motorB: %d", motor[motorB]);
	displayCenteredTextLine(5, "motorC: %d", motor[motorC]);
}

void showBtInput()
{
	displayCenteredTextLine(0, "input 1: %d", x);
	displayCenteredTextLine(1, "input 2: %d", y);
	displayCenteredTextLine(2, "input 3: %d", z);
}

void rotateClockWise(int speed)
{
	motor[motorA] = sqrt(3)/2*speed;
	motor[motorB] = sqrt(3)/2*speed;
	motor[motorC] = speed;
	while ((nMotorEncoder[motorC] < 1400) && (nMotorEncoder[motorA] < (1400 * 0.86)) && (nMotorEncoder[motorB] < 1400 * 0.86))
		wait1Msec(1);
	pause();
	return;
}

void rotateCounterClockWise(int speed)
{
	motor[motorA] = -sqrt(3)/2*speed;
	motor[motorB] = -sqrt(3)/2*speed;
	motor[motorC] = -speed;
	return;
}

void go(int angle, int speed)
{
	motor[motorA] = speed * cosDegrees(angle-angleA);
	motor[motorB] = speed * cosDegrees(angle-angleB);
	motor[motorC] = speed * cosDegrees(angle-angleC);
	return;
}

/*
    ^ B
   /   \
	A     v
		<-C
*/

task main()
{
	rotateClockWise(100);
	showSpeed();
	wait10Msec(100000);
}
