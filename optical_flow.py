#!/usr/bin/python
# -*- coding: utf-8 -*-

# Python 2/3 compatibility
from __future__ import print_function

import numpy as np
import cv2
import time
from statistics import mode

help_message = \
    '''
USAGE: optical_flow.py [<video_source>]
Keys:
 1 - toggle HSV flow visualization
 2 - toggle glitch
'''
count = 0

def most_frequent(List): 
    dict = {} 
    count, itm = 0, '' 
    for item in reversed(List): 
        dict[item] = dict.get(item, 0) + 1
        if dict[item] >= count : 
            count, itm = dict[item], item
    return(itm, count)

def draw_flow(img, flow, step=16):
    
    #from the beginning to position 2 (excluded channel info at position 3)
    h, w = img.shape[:2]
    y, x = np.mgrid[step/2:h:step, step/2:w:step].reshape(2,-1).astype(int)
    fx, fy = flow[y,x].T
    lines = np.vstack([x, y, x+fx, y+fy]).T.reshape(-1, 2, 2)
    lines = np.int32(lines + 0.5)
    vis = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    cv2.polylines(vis, lines, 0, (0, 255, 0))
    for (x1, y1), (x2, y2) in lines:
        cv2.circle(vis, (x1, y1), 1, (0, 255, 0), -1)
    return vis

def draw_hsv(flow):
    (h, w) = flow.shape[:2]
    (fx, fy) = (flow[:, :, 0], flow[:, :, 1])
    ang = np.arctan2(fy, fx) + np.pi
    v = np.sqrt(fx * fx + fy * fy)
    hsv = np.zeros((h, w, 3), np.uint8)
    hsv[..., 0] = ang * (180 / np.pi / 2)
    hsv[..., 1] = 0xFF
    hsv[..., 2] = np.minimum(v * 4, 0xFF)
    bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    cv2.imshow('hsv', bgr)
    return bgr

if __name__ == '__main__':
    import sys
    print (help_message)
    try:
        fn = sys.argv[1]
    except:
        fn = 0

    cam = cv2.VideoCapture(fn)
    (ret, prev) = cam.read()

    prevgray = cv2.cvtColor(prev, cv2.COLOR_BGR2GRAY)
    show_hsv = True
    show_glitch = False
    cur_glitch = prev.copy()

    rects = []
    img = []
    while True:
        timer = cv2.getTickCount()

        (ret, img) = cam.read()
        vis = img.copy()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        flow = cv2.calcOpticalFlowFarneback(prevgray,gray,None,0.5,5,15,3,5,1.1,cv2.OPTFLOW_FARNEBACK_GAUSSIAN)
        prevgray = gray
        if show_hsv:
            gray1 = cv2.cvtColor(draw_hsv(flow), cv2.COLOR_BGR2GRAY)
            thresh = cv2.threshold(gray1, 50, 0xFF,
                                   cv2.THRESH_BINARY)[1]
            # thresh = cv2.dilate(thresh, None, iterations=2)
            # thresh = cv2.morphologyEx(thresh.copy(), cv2.MORPH_OPEN, np.ones((5,5),np.uint8))
            # thresh = cv2.morphologyEx(thresh.copy(), cv2.MORPH_CLOSE, np.ones((5,5),np.uint8))
            cv2.imshow('thresh', thresh)
            gray2, cnts, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            # loop over the contours
            prevlen = len(cnts)
            for idx, c in enumerate(cnts):
                # if the contour is too small, ignore it
                (x, y, w, h) = cv2.boundingRect(c)
                if not (w > 100 and h > 100 and w < 900 and h < 680):
                    del cnts[idx]
            print(prevlen-len(cnts))

            if (len(cnts) > 0):
                (x,y,w,h) = cv2.boundingRect(np.vstack(cnts))
                rects.append((x,y,w,h))
                cv2.rectangle(vis, (x, y), (x + w, y + h), (0, 0xFF, 0), 4)
                cv2.imshow('Image', vis)

        ch = 0xFF & cv2.waitKey(1)
        if ch == 27:
            break
        if ch == ord('1'):
            show_hsv = not show_hsv
            print ('HSV flow visualization is', ['off', 'on'][show_hsv])
        fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer)
        # print(fps)
    cv2.destroyAllWindows()
    smallRects = []
    for rect in rects:
        smallRects.append((rect[0] // 10, rect[1] // 10, rect[2] // 10, rect[3] // 10))

    mostCommonRect, cnt = most_frequent(smallRects)
    print(cnt)
    x,y,w,h = mostCommonRect
    cv2.rectangle(img, (x*10, y*10), ((x+w)*10, (y+h)*10), (0,0,255), 3)
    x = 0; y = 0; w = 0; h = 0
    for rect in rects:
        x += rect[0]
        y += rect[1]
        w += rect[2]
        h += rect[3]

    x //= len(rects)
    y //= len(rects)
    w //= len(rects)
    h //= len(rects)
        
    cv2.rectangle(img, (x, y), (x+w, y+h), (0,255,0), 3)
    cv2.imshow("result", img)
    cv2.waitKey(0)