# General approach to tracking

Have a separate tracker with same settings, but different starting bbox, for each robot.

If the tracker fails, use objdetect to find it again and start over the tracker. How do I figure out that tracker has failed tho? Maybe run objdetect every couple of secs to check if the thing it finds is close to what tracking finds.