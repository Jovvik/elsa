def create_cat_tf_example(encoded_cat_image_data):
    """Creates a tf.Example proto from sample cat image.

    Args:
    encoded_cat_image_data: The jpg encoded data of the cat image.

    Returns:
    example: The created tf.Example.
    """

    height = 1032.0
    width = 1200.0
    filename = 'example_cat.jpg'
    image_format = b'jpg'

    xmins = [322.0 / 1200.0]
    xmaxs = [1062.0 / 1200.0]
    ymins = [174.0 / 1032.0]
    ymaxs = [761.0 / 1032.0]
    classes_text = ['Cat']
    classes = [1]

    tf_example = tf.train.Example(features=tf.train.Features(feature={
        'image/height': dataset_util.int64_feature(height),
        'image/width': dataset_util.int64_feature(width),
        'image/filename': dataset_util.bytes_feature(filename),
        'image/source_id': dataset_util.bytes_feature(filename),
        'image/encoded': dataset_util.bytes_feature(encoded_image_data),
        'image/format': dataset_util.bytes_feature(image_format),
        'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
        'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
        'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
        'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
        'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
        'image/object/class/label': dataset_util.int64_list_feature(classes),
    }))
    return tf_example

import tensorflow as tf
import cv2

from object_detection.utils import dataset_util

flags = tf.app.flags
flags.DEFINE_string('output_path', '', 'Path to output TFRecord')
FLAGS = flags.FLAGS

def create_tf_example(example):
  map = {
    1:'zhmih',
    2:'zhizha',
    3:'nya'
  }
  # TODO(user): Populate the following variables from your example.
  height = 480 # Image height
  width = 640 # Image width
  filename = example['filename'] # Filename of the image. Empty if image is not from file
  with tf.gfile.GFile('dnnImgs/' + filename, 'rb') as fid:
    encoded_jpg = fid.read()
  encoded_image_data = encoded_jpg # Encoded image bytes
  image_format = b'jpeg' # b'jpeg' or b'png'

  xmins = [] # List of normalized left x coordinates in bounding box (1 per box)
  xmaxs = [] # List of normalized right x coordinates in bounding box
             # (1 per box)
  ymins = [] # List of normalized top y coordinates in bounding box (1 per box)
  ymaxs = [] # List of normalized bottom y coordinates in bounding box
             # (1 per box)
  classes_text = [] # List of string class name of bounding box (1 per box)
  classes = [] # List of integer class id of bounding box (1 per box)
  
  for region in example['regions']:
    shape_attribs = region['shape_attributes']
    region_attribs = region['region_attributes']
    xmins.append(shape_attribs['x'] / width)
    xmaxs.append((shape_attribs['x'] + shape_attribs['width']) / width)
    ymins.append(shape_attribs['y'] / height)
    ymaxs.append((shape_attribs['y'] + shape_attribs['height']) / height)

    if len(region_attribs) == 0:
      return None
    if region_attribs['class'] == '':
      return None
    classes.append(int(region_attribs['class']))
    classes_text.append(map[classes[-1]])

  print(xmins, xmaxs, ymins, ymaxs, image_format, height, width, filename, classes, classes_text)
  tf_example = tf.train.Example(features=tf.train.Features(feature={
      'image/height': dataset_util.int64_feature(height),
      'image/width': dataset_util.int64_feature(width),
      'image/filename': dataset_util.bytes_feature(bytes(filename, 'utf-8')),
      'image/source_id': dataset_util.bytes_feature(bytes(filename, 'utf-8')),
      'image/encoded': dataset_util.bytes_feature(encoded_image_data),
      'image/format': dataset_util.bytes_feature(image_format),
      'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
      'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
      'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
      'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
      'image/object/class/text': dataset_util.bytes_list_feature([bytes(x, 'utf-8') for x in classes_text]),
      'image/object/class/label': dataset_util.int64_list_feature(classes),
  }))
  return tf_example

import os
import json

def main(_):
  writer = tf.python_io.TFRecordWriter('dataset.record')

  # TODO(user): Write code to read in your dataset to examples variable

  with open('dataset_v2/filtered_via_region_data.json', 'r') as f:
    data = json.load(f)
  
  for img in data:
    if len(data[img]['regions']) != 0:
      example = create_tf_example(data[img])
      if example is not None:
        writer.write(example.SerializeToString())

  writer.close()


if __name__ == '__main__':
  tf.app.run()