import cv2

cap = cv2.VideoCapture(1)
while True:
    timer = cv2.getTickCount()
    ok, frame = cap.read()
    cv2.imshow('frame', frame)
    fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer)
    print(fps)
    cv2.waitKey(1)