import cv2

lowerBound = 0
upperBound = 500

def nothing(a):
    pass

cap = cv2.VideoCapture(0)

cv2.namedWindow("img")
cv2.createTrackbar("H1", "img", 0, 500, nothing)
cv2.createTrackbar("S1", "img", 0, 500, nothing)

while True:
    ok, frame = cap.read()
    frame = cv2.GaussianBlur(frame, (3, 3), 0, 0)
    print(lowerBound, upperBound)
    edges = cv2.Canny(frame, lowerBound, upperBound)
    cv2.imshow('frame', frame)
    cv2.imshow('edges', edges)
    if cv2.waitKey(1) == 27:
        lowerBound = int(input())
        upperBound = int(input())