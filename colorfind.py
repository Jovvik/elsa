# -*- coding: utf-8 -*-

'''
Color-based object detection in python
Created by Maxim Mikhaylov, 2018
'''

import cv2
import numpy as np
from sys import argv

cap = cv2.VideoCapture(0)

def nothing(x):
    pass

def loadScalars(filename):
	if filename == []:
		return [[0, 0, 0], [0, 0, 0]]
	f = open(filename[0], 'r')
	scalars = []
	for line in f:
		scalars.append([int(s) for s in line.split() if s.isdigit()])
	return scalars

def colorfind(img, lowerBound, upperBound, lowerBound2=None, upperBound2=None, drawPos=False, showMask=False, threshold=1):
	'''
	Находит в изображении img объект на основе его цвета. 
	
	Цвет задается границами lowerBound и upperBound.
	
	lowerBound2 и upperBound2 используются в случае, если искомый цвет - красный, т.к. он "обворачивается" вокруг HSV кольца.
	
	drawPos - рисует круг, где находится объект и показывает результируеще изображение.
	
	threshold - число пикселей искомого цвета, при котором считается, что объект есть в изображении.
	
	Если искомых пикселей меньше чем threshold, то возвращается (-1, -1). 
	
	Иначе - кортеж из координат объекта.
	'''

	# Перевод изображения в HSV
	imgHSV = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

	# Создание бинарного изображения - маски, где пиксель имеет значение 1, 
	# если на его месте в оригинальном изображении пискель цвета между upperBound и lowerBound
	mask = cv2.inRange(imgHSV, lowerBound, upperBound)
	# Дополнительная фильтрация, если использются две пары границ
	if lowerBound2 is not None and upperBound2 is not None:
		mask += cv2.inRange(imgHSV, lowerBound2, upperBound2)

	# Удаление отдельно стоящих точек из маски
	mask = cv2.erode(mask, np.ones((5,5)), iterations=1)
	mask = cv2.dilate(mask, np.ones((5,5)), iterations=1)
	mask = cv2.dilate(mask, np.ones((5,5)), iterations=1)
	mask = cv2.erode(mask, np.ones((5,5)), iterations=1)

	# Создание моментов
	moments = cv2.moments(mask)

	# Нахождение координат объекта
	if moments['m00'] < threshold:
		return (-1, -1)	
	cx = int(moments['m10'] / moments['m00'])
	cy = int(moments['m01'] / moments['m00'])

	if showMask:
		cv2.imshow("mask", mask)
		cv2.waitKey(1)

	if drawPos:
		cv2.circle(img, (cx, cy), 10, (0,0,255), 3)
		cv2.imshow("img", img)
		cv2.waitKey(1)
	
	return (cx, cy)

if __name__ == "__main__":
	
	res = loadScalars(argv[1:])

	# Трэкбары для управления границамии
	cv2.namedWindow("img")
	cv2.createTrackbar("H1", "img", res[0][0], 180, nothing)
	cv2.createTrackbar("S1", "img", res[0][1], 255, nothing)
	cv2.createTrackbar("V1", "img", res[0][2], 255, nothing)
	cv2.createTrackbar("H2", "img", res[1][0], 180, nothing)
	cv2.createTrackbar("S2", "img", res[1][1], 255, nothing)
	cv2.createTrackbar("V2", "img", res[1][2], 255, nothing)

	while True:
		# Считывание изображения с камеры
		a, img = cap.read()

		lowerBound = []
		upperBound = []
		
		# Считывание данных с трекбаров
		lowerBound.append(cv2.getTrackbarPos('H1', 'img'))
		lowerBound.append(cv2.getTrackbarPos('S1', 'img'))
		lowerBound.append(cv2.getTrackbarPos('V1', 'img'))

		upperBound.append(cv2.getTrackbarPos('H2', 'img'))
		upperBound.append(cv2.getTrackbarPos('S2', 'img'))
		upperBound.append(cv2.getTrackbarPos('V2', 'img'))

		colorfind(img, np.asarray(lowerBound), np.asarray(upperBound), drawPos=True, showMask=True, threshold=5000)
		key = cv2.waitKey(1)
		# Выход из программы по нажатию esc
		if key == 27:
			exit()
		# Сохранение скаляров
		if key > 0:
			print("where to save the scalars?")
			print("the path must be in '  ' ")
			savepath = str(input())
			f = open(savepath, 'w')
			f.write(' '.join(str(e) for e in lowerBound) + '\n')
			f.write(' '.join(str(e) for e in upperBound))
			f.close()