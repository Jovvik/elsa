""" 
Отче,
да святится имя Твое;
да приидет Царствие Твое;
хлеб наш насущный подавай нам на каждый день;
и прости нам грехи наши,
ибо и мы прощаем всякому должнику нашему;
и не введи нас в искушение,
но избавь нас от лукавого. 
Ибо Твое есть Царство и сила,
и слава во веки. Аминь.
"""

MINDIST = 50 # minimal deviation in pix for NXT to move
COMMAND_FREQ = 0.1 # 1 sec between commands to NXTs
SPEED_COEF = 0.5 # speed per pixel of dist

import cv2
import sys
from time import time, sleep
import numpy as np
from math import atan2, pi, sqrt
from random import randint
from copy import copy

import serial

class NXT():
    def __init__(self, port, baud=9600):
        self.port = port
        self.ser = serial.Serial(port, baud)
        self.ready = False
        self.timeSinceLastComm = cv2.getTickCount()
    
    def send(self, vals_):
        try:
            vals = vals_.copy()
            for idx, val in enumerate(vals):
                vals[idx] = hex(val)[2:].zfill(4)
            buf = b'\x0A\x00\x80\x09\x00\x06'
            for val in vals:
                buf += bytes.fromhex(val[2:]) + bytes.fromhex(val[:-2])
            self.ser.write(buf)
            self.timeSinceLastComm = cv2.getTickCount()
        except:
            print('Failed at sending')
            self.reconnect()
    
    def reconnect(self):
        try:
            self.ser.__del__()
        except:
            print('Failed at deleting')
        self.ser = serial.Serial(self.port)
    
    def rotate(self):
        vals = [0, 0, 1]
        self.send(vals)

    def stop(self):
        vals = [0, 0, 2]
        self.send(vals)

def randColor():
    return (randint(0, 255), randint(0, 255), randint(0, 255))

def midOfRect(rect):
    return (int(rect[2] + 2*rect[0]) // 2, int(rect[3] + 2*rect[1]) // 2)

class trackerWrapper():
    def __init__(self, name, nxt, color=(255,0,0), drawPath=False):
        self.name = name
        self.color = color
        self.drawPath = drawPath
        self.nxt = nxt
        if name == 'BOOSTING':
            self.tracker = cv2.TrackerBoosting_create()
        elif name == 'MIL':
            self.tracker = cv2.TrackerMIL_create()
        elif name == 'KCF':
            self.tracker = cv2.TrackerKCF_create()
        elif name == 'TLD':
            self.tracker = cv2.TrackerTLD_create()
        elif name == 'MEDIANFLOW':
            self.tracker = cv2.TrackerMedianFlow_create()
        elif name == 'GOTURN': #! Warning! Requires stuff from here: https://github.com/Mogball/goturn-files
            self.tracker = cv2.TrackerGOTURN_create()
        elif name == 'MOSSE':
            self.tracker = cv2.TrackerMOSSE_create()
        elif name == "CSRT":
            self.tracker = cv2.TrackerCSRT_create()
        else:
            raise Exception()

    def init(self, frame, bbox):
        self.pos = midOfRect(bbox)
        self.firstpos = midOfRect(bbox)
        if self.drawPath:
            self.path = np.zeros_like(frame)
        return self.tracker.init(frame, bbox)

    def update(self, frame):
        ok, bbox = self.tracker.update(frame)
        self.ok = ok
        self.bbox = bbox
        self.prevpos = self.pos
        self.pos = midOfRect(self.bbox)
        if self.drawPath:
            cv2.line(self.path, self.prevpos, self.pos, color=self.color, thickness=1)

class multiObjTracker():
    def __init__(self, trackerType):
        self.trackers = []
        self.trackerType = trackerType
    
    def addTracker(self, nxt, bbox, frame):
        self.trackers.append(trackerWrapper(self.trackerType, nxt, color=randColor()))
        self.trackers[-1].init(frame, bbox)

    def addBboxes(self, bboxes):
        self.bboxes = bboxes

    def init(self, frame, bboxes):
        self.bboxes = bboxes
        for idx, bbox in enumerate(bboxes):
            self.trackers[idx].init(frame, bbox)
            self.trackers[idx].name+=str(idx)

    def update(self, frame):
        for idx, tracker in enumerate(self.trackers):
            tracker.update(frame)
            # print(angle)

def draw_hsv(flow):
    (h, w) = flow.shape[:2]
    (fx, fy) = (flow[:, :, 0], flow[:, :, 1])
    ang = np.arctan2(fy, fx) + np.pi
    v = np.sqrt(fx * fx + fy * fy)
    hsv = np.zeros((h, w, 3), np.uint8)
    hsv[..., 0] = ang * (180 / np.pi / 2)
    hsv[..., 1] = 0xFF
    hsv[..., 2] = np.minimum(v * 4, 0xFF)
    bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    cv2.imshow('hsv', bgr)
    return bgr

def findNXT(nxt, cap):
    nxt.rotate()

    ok, prev = cap.read()
    prevgray = cv2.cvtColor(prev, cv2.COLOR_BGR2GRAY)

    startTime = time()
    currTime = time()
    rects = []
    while currTime - startTime < 3:
        ok, frame = cap.read()
        vis = frame.copy()

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        flow = cv2.calcOpticalFlowFarneback(prevgray,gray,None,0.5,5,15,3,5,1.1,cv2.OPTFLOW_FARNEBACK_GAUSSIAN)
        prevgray = gray

        gray1 = cv2.cvtColor(draw_hsv(flow), cv2.COLOR_BGR2GRAY)
        thresh = cv2.threshold(gray1, 25, 0xFF, cv2.THRESH_BINARY)[1]
        cv2.imshow('thresh', thresh)
        
        gray2, cnts, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        for idx, c in enumerate(cnts):
            x, y, w, h = cv2.boundingRect(c)
            if not (w > 100 and h > 100 and w < 900 and h < 700):
                del cnts[idx]
        
        if len(cnts) > 5:
            x, y, w, h = cv2.boundingRect(np.vstack(cnts))
            rects.append((x,y,w,h))
            cv2.rectangle(vis, (x, y), (x + w, y + h), (0, 0xFF, 0), 4)
            cv2.imshow('Visualisation', vis)
        cv2.waitKey(1)
    
        currTime = time()
    
    if len(rects) > 5:
        x = 0; y = 0; w = 0; h = 0
        goodRectCnt = 0
        for rect in rects:
            if rect[2] > 50 and rect[3] > 50 and rect[2] < 900 and rect[3] < 700: # rect size is ok
                goodRectCnt += 1
                x += rect[0]
                y += rect[1]
                w += rect[2]
                h += rect[3]

        if x is not 0 and y is not 0 and w is not 0 and h is not 0:
            x //= goodRectCnt
            y //= goodRectCnt
            w //= goodRectCnt
            h //= goodRectCnt
            
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0,255,0), 3)
            cv2.imshow("result" + nxt.port[-1], frame)
            # cv2.waitKey(0)

            nxt.ready = True
            return (True, (x,y,w,h), frame)

    nxt.ready = False
    print("NXT# " + nxt.port[-1] + " not found, I'll try to find it soon")
    return (False, None, None)

NXTs = [NXT('/dev/rfcomm0'),
        NXT('/dev/rfcomm1'),
        NXT('/dev/rfcomm2')
        ]

sleep(5)

# Read video capture
cap = cv2.VideoCapture(1)

# Exit if video capture not opened
if not cap.isOpened():
    print ("Could not open video capture")
    sys.exit()

# Read first frame
ok, frame = cap.read()
if not ok:
    print ('Cannot read frame')
    sys.exit()


bboxes = []
# for _ in range(objnum):
#     bbox = cv2.selectROI(frame, False)
#     bboxes.append(bbox)

mot = multiObjTracker('MEDIANFLOW')

for nxt in NXTs:
    ok, bbox, lastFrame = findNXT(nxt, cap)
    if ok:
        print(ok, bbox)
        bboxes.append(bbox)
    else:
        bboxes.append((0,0,0,0))
        nxt.reconnect()

for idx, nxt in enumerate(NXTs):
    if nxt.ready:
        mot.addTracker(nxt, bboxes[idx], lastFrame)
# mot.addBboxes(bboxes)

cv2.waitKey(0)

while True:
    # Read a new frame
    ok, frame = cap.read()
    if not ok:
        break
        
    # Start timer
    timer = cv2.getTickCount()

    # Update tracker
    mot.update(frame)
    # for tracker in trackers:
    #     tracker.update(frame)

    # Calculate FPS
    fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer)

    for idx, tracker in enumerate(mot.trackers):
        ok = tracker.ok
        bbox = tracker.bbox
        # Draw bounding box
        if ok:
            # Tracking success
            p1 = (int(bbox[0]), int(bbox[1]))
            p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
            cv2.arrowedLine(frame, tracker.firstpos, tracker.pos, tracker.color)
            cv2.rectangle(frame, p1, p2, tracker.color, 2, 1)
            # cv2.imshow("Path for " + tracker.name, tracker.path)

            diff = tracker.firstpos[1] - tracker.pos[1],  -tracker.firstpos[0] + tracker.pos[0]
            angle = atan2(diff[0], diff[1]) / pi * 180
            if angle < 0:
                angle += 360
            angle += 180
            angle %= 360
            dist = sqrt(diff[0] ** 2 + diff[1] ** 2)
            if (cv2.getTickCount() - tracker.nxt.timeSinceLastComm) / cv2.getTickFrequency() > COMMAND_FREQ and dist > MINDIST:
                tracker.nxt.send([int(angle), min(int(dist * SPEED_COEF), 100), int(COMMAND_FREQ * 10)])
                print(int(angle), min(int(dist * SPEED_COEF), 100), int(COMMAND_FREQ * 10))
                tracker.nxt.timeSinceLastComm = cv2.getTickCount()
        else:
            # Tracking failure
            tracker.nxt.stop()
            tracker.nxt.ready = False
            print("NXT #" + tracker.nxt.port[-1] + " stopped")
            del mot.trackers[idx]
            print(len(mot.trackers))
            print(len(NXTs))
            # cv2.putText(frame, "Tracking failure detected in " + tracker.name, (100,80), cv2.FONT_HERSHEY_SIMPLEX, 0.75,(0,0,255),2)

    # Display tracker type on frame
    # cv2.putText(frame, tracker_type + " Tracker", (100,20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50),2)
    
    # Display FPS on frame
    cv2.putText(frame, "FPS : " + str(int(fps)), (100,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50), 2)

    # if (len(mot.trackers) == 0):
    #     for nxt in NXTs:
    #         nxt.rotate()

    # Display result
    cv2.imshow("Tracking", frame)
    # cv2.imshow("Path", movementMap)

    # Exit if ESC pressed
    k = cv2.waitKey(1) & 0xff
    if k == 27 : break
    if k == ord('a'):
        for idx, nxt in enumerate(NXTs):
            if not nxt.ready:
                ok, bbox, lastFrame = findNXT(nxt, cap)
                if ok:
                    print(ok, bbox)
                    mot.addTracker(nxt, bbox, lastFrame)

for nxt in NXTs:
    nxt.stop()