CC = g++
CFLAGS = -std=c++11
BUILDFLDR = bin

OPENCV = `pkg-config --cflags --libs opencv`
LIBS = $(OPENCV)

rfcomm:
	sudo rfcomm bind 0 00:16:53:1A:AA:E7
	sudo rfcomm bind 1 00:16:53:1B:06:FA
	sudo rfcomm bind 2 00:16:53:0F:26:BA

main:
	python3 main.py

tracking:
	python3 tracking.py

run: | build
	bin/tracking

build:
	$(CC) tracking.cpp -o bin/tracking $(LIBS) $(CFLAGS)