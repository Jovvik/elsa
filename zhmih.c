float angleA;
float angleB;
float angleC;

long x, y, z;

void showSpeed()
{
	displayCenteredTextLine(3, "motorA: %d", motor[motorA]);
	displayCenteredTextLine(4, "motorB: %d", motor[motorB]);
	displayCenteredTextLine(5, "motorC: %d", motor[motorC]);
}

void showBtInput()
{
	displayCenteredTextLine(0, "input 1: %d", x);
	displayCenteredTextLine(1, "input 2: %d", y);
	displayCenteredTextLine(2, "input 3: %d", z);
}

void rotateClockWise(int speed)
{
	motor[motorA] = sqrt(3)/2*speed;
	motor[motorB] = sqrt(3)/2*speed;
	motor[motorC] = speed;
	return;
}

void rotateCounterClockWise(int speed)
{
	motor[motorA] = -sqrt(3)/2*speed;
	motor[motorB] = -sqrt(3)/2*speed;
	motor[motorC] = -speed;
	return;
}

void go(int angle, int speed)
{
	motor[motorA] = speed * cosDegrees(angle-angleA);
	motor[motorB] = speed * cosDegrees(angle-angleB);
	motor[motorC] = speed * cosDegrees(angle-angleC);
	return;
}

void pause()
{
	motor[motorA]=0;
	motor[motorB]=0;
	motor[motorC]=0;
}

/*
    ^ B
   /   \
	A     v
		<-C
*/

task main()
{
	long old_z;
	pause();

	angleA = 60;
	angleB = -60;
	angleC = 180;

  string displayx, displayy, displayz;

	while(1)
  {
		do
		{
			if (time1[T1] > 100 * old_z)
			{
				pause();
				showSpeed();
			}
			x = messageParm[0];
			y = messageParm[1];
			z = messageParm[2];
			wait10Msec(1);
			ClearMessage();
		} while (y == 0);
		showBtInput();
		old_z = z;

		go(x,y);
		showSpeed();
		clearTimer(T1);
	}
}
